/*
 * Lab 2 Big Positive Integer
 * Name:
 * Lab Section(Tuesday / Thursday):
 */
#include "iostream"
#include "string"
#include "bigPosInteger.h"
using namespace std;

bigPosInteger::bigPosInteger()
{ storeLength=0,Length=0,storeoutputLength=0,outputLength=0;
    valueArray=NULL;
}

bigPosInteger::bigPosInteger(std::string value)
/*This constructor should take in a string containing a set of chars between '0' and '9' of arbitrary length and constructs it into bigPosInteger type*/
{
    std::string str(value);
    Length = value.size();
    valueArray = new int[Length];
    storeLength=Length;
    storeoutputLength=0,outputLength=0;
    for(int k=0; k<Length;k++ ){
        valueArray[k] = (value[k] - '0');
    }
}

bigPosInteger::bigPosInteger(int value) //Come back to this.
/*this instructor takes in an integer and constructs it into a bigPosInteger type*/
{
    Length=value;
    valueArray=new int[Length];
    for(int k=0;k<Length;k++){
        valueArray[k] = 0;
    }
}

bigPosInteger::bigPosInteger(const bigPosInteger& value)
/*This is a copy constructor, be EXTREMELY careful for memory leaks here*/
{
    storeLength= value.storeLength;
    Length = value.Length;
    valueArray=new int[Length];
    if (this == &value) {return;}
    else{
        if (valueArray != NULL)
            delete []this->valueArray;
        this->Length=value.storeoutputLength;
        valueArray= new int [Length];
        for(int i=0; i<Length; ++i)
            this->valueArray[i] = value.valueArray[i];
    }
}


bigPosInteger::~bigPosInteger()
/*This is the destructor, be extremely careful for memory leaks here*/
{
    delete [] valueArray;
}

bigPosInteger bigPosInteger::operator+ (const bigPosInteger& rhs)
/*this operator should be able to add two bigPosInteger together and return the result. The default return should be replaced with the appropriate variable*/
{
    if(this->storeLength < rhs.storeLength){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [rhs.storeLength];
            for(int i=0; i<=rhs.storeLength; ++i){
                outcome.valueArray[i] = 0;}}
        outcome.outputLength = rhs.storeLength;
        outcome.storeoutputLength=outcome.outputLength;
        outcome.Length=this->storeLength;
        outcome.storeLength=this->storeLength;
        while(outcome.Length>0){
            outcome.valueArray[outcome.outputLength]= this->valueArray[outcome.Length-1] + rhs.valueArray[outcome.outputLength-1]+outcome.valueArray[outcome.outputLength];
        if(outcome.valueArray[outcome.outputLength]>=10){
            outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]%10;
            outcome.valueArray[outcome.outputLength-1]= outcome.valueArray[outcome.outputLength-1]+1;
        }
            outcome.Length--;
            outcome.outputLength--;
        }
        for(int i=0;outcome.outputLength>i;outcome.outputLength--){
            outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]+rhs.valueArray[outcome.outputLength-1];
            if(outcome.valueArray[outcome.outputLength]>=10){
                outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]%10;
                outcome.valueArray[outcome.outputLength-1]= outcome.valueArray[outcome.outputLength-1]+1;
            }
        }
        outcome.Length=outcome.storeLength;
        outcome.outputLength=outcome.storeoutputLength;
        outcome.storeoutputLength=rhs.storeLength+1;
        return outcome;


    }
    if(rhs.storeLength < this->storeLength){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=rhs.storeLength;
            outcome.valueArray= new int [this->storeLength];
            for(int i=0; i<=this->storeLength; ++i){
                outcome.valueArray[i] = 0;}}

        outcome.outputLength = this->storeLength;
        outcome.storeoutputLength=outcome.outputLength+1;
        outcome.Length=rhs.storeLength;
        outcome.storeLength=rhs.storeLength;
        while(outcome.Length>0){
            outcome.valueArray[outcome.outputLength]= this->valueArray[outcome.outputLength-1] + rhs.valueArray[outcome.Length-1]+outcome.valueArray[outcome.outputLength];
            if(outcome.valueArray[outcome.outputLength]>=10){
                outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]%10;
                outcome.valueArray[outcome.outputLength-1]= outcome.valueArray[outcome.outputLength-1]+1;
            }
            outcome.Length--;
            outcome.outputLength--;
        }
        for(int i=0;i<outcome.outputLength;outcome.outputLength--){
            outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]+this->valueArray[outcome.outputLength-1];
            if(outcome.valueArray[outcome.outputLength]>=10){
                outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]%10;
                outcome.valueArray[outcome.outputLength-1]= outcome.valueArray[outcome.outputLength-1]+1;
            }
        }
        outcome.Length=outcome.storeLength;
        outcome.outputLength=outcome.storeoutputLength;
        outcome.storeoutputLength=this->storeLength+1;
        return outcome;}


    if(rhs.storeLength == this->storeLength){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
        outcome.Length=rhs.storeLength;
        outcome.valueArray= new int [Length];
        for(int i=0; i<=outcome.Length; ++i){
            outcome.valueArray[i] = 0;}}
        outcome.outputLength = this->storeLength;
        outcome.storeoutputLength=outcome.outputLength+1;
        outcome.Length=rhs.storeLength;
        outcome.storeLength=rhs.storeLength; //Organize and store the both Lengths for use on class outcome;

        while(outcome.outputLength>=1){
            outcome.valueArray[outcome.outputLength]= this->valueArray[outcome.outputLength-1] + rhs.valueArray[outcome.outputLength-1]+outcome.valueArray[outcome.outputLength];
            if(outcome.valueArray[outcome.outputLength]>=10){
                outcome.valueArray[outcome.outputLength]=outcome.valueArray[outcome.outputLength]%10;
                outcome.valueArray[outcome.outputLength-1] = outcome.valueArray[outcome.outputLength-1]+1;
            }outcome.outputLength--;
        }
        outcome.outputLength=outcome.storeoutputLength;
        return outcome;}
    }

bigPosInteger bigPosInteger::operator- (const bigPosInteger& rhs)
/*this operator should be able to subtract the Right Hand Side bigPosInteger from the base bigPosInteger and return the result. The default return should be replaced with the appropriate variable*/
{
  if(this->storeLength<rhs.storeLength||((this->storeLength==rhs.storeLength)&&(this->valueArray[0] < rhs.valueArray[0]))){
  std::cout<<"NEGATIVE ERROR: Failed to compute due to outcome being negative."<<std::endl;
}
    else{
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [this->storeLength];
            for(int i=0; i<this->storeLength; ++i){
                outcome.valueArray[i] = this->valueArray[i];}}
        outcome.outputLength = this->storeLength;
        outcome.storeoutputLength=outcome.outputLength;
        outcome.Length=rhs.storeLength;
        outcome.storeLength=this->storeLength;

      while(outcome.Length>0){

          if(this->valueArray[outcome.outputLength-1]<rhs.valueArray[outcome.Length-1]) {
              for (int i = 1; this->valueArray[outcome.outputLength-(i+1)]<= 0; i++) {
              outcome.valueArray[outcome.outputLength-(i+1)]=-9;
                  if(this->valueArray[outcome.outputLength-(i+2)>0]){
                      outcome.valueArray[outcome.outputLength-(i+2)]=outcome.valueArray[outcome.outputLength-(i+2)]-1;
                      break;
                  }
              }

              for (int i = 0; i<outcome.storeoutputLength;i++){
                  if(outcome.valueArray[i]==-9){
                      outcome.valueArray[i]=9;
                  }
              }
              outcome.valueArray[outcome.outputLength-1]= (outcome.valueArray[outcome.outputLength-1]+10)-rhs.valueArray[outcome.Length-1];
          }  else{
              outcome.valueArray[outcome.outputLength-1]=this->valueArray[outcome.outputLength-1]-rhs.valueArray[outcome.Length-1];
          }
    outcome.Length--;
          outcome.outputLength--;
      }
      return outcome;
  }}

bigPosInteger bigPosInteger::operator*(const bigPosInteger& rhs)
/*this operator should be able to multiply two bigPosInteger together and return the result. The default return should be replaced with the appropriate variable*/
{   int storerhs=0, storelhs=0, multiplied=0;
    int lengthlhs=this->Length-1;
    int lengthrhs=rhs.Length-1;
    int multiplier=1;

    for(int i=0; i<=lengthlhs; lengthlhs--){
        storelhs= storelhs + (this->valueArray[lengthlhs]*multiplier);
        multiplier=multiplier*10;
    }
    multiplier=1;
    for(int i=0; i<=lengthrhs; lengthrhs--){
        storerhs= storerhs + (rhs.valueArray[lengthrhs]*multiplier);
        multiplier=multiplier*10;
    }
    multiplier=1;
    multiplied=storerhs*storelhs;
    std::cout<<multiplied<<std::endl;
    if(this->Length<rhs.Length){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=rhs.storeLength;
            outcome.valueArray= new int [(rhs.Length*2)];
            int newLength= (rhs.Length*2);
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (multiplied%(multiplier*10))/multiplier;
                multiplied=multiplied-(multiplied%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=rhs.Length*2;
        return outcome;
    }
    if(rhs.Length<this->Length){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [(this->Length*2)];
            int newLength= (this->Length*2);
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (multiplied%(multiplier*10))/multiplier;
                multiplied=multiplied-(multiplied%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=this->Length*2;
        return outcome;
    }
    else{
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [(this->Length*2)+1];
            int newLength= (this->Length*2)+1;
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (multiplied%(multiplier*10))/(multiplier);
                multiplied=multiplied-(multiplied%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=(this->Length*2)+1;
        return outcome;
    }
}
bigPosInteger bigPosInteger::operator%(const bigPosInteger &rhs) {
    int storerhs=0, storelhs=0, modulus=0;
    int lengthlhs=this->Length-1;
    int lengthrhs=rhs.Length-1;
    int multiplier=1;

    for(int i=0; i<=lengthlhs; lengthlhs--){
        storelhs= storelhs + (this->valueArray[lengthlhs]*multiplier);
        multiplier=multiplier*10;
    }
    multiplier=1;
    for(int i=0; i<=lengthrhs; lengthrhs--){
        storerhs= storerhs + (rhs.valueArray[lengthrhs]*multiplier);
        multiplier=multiplier*10;
    }
    multiplier=1;
    modulus=storelhs%storerhs;
    std::cout<<modulus<<std::endl;
    if(this->Length<rhs.Length){

    }
    if(rhs.Length<this->Length){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [(this->Length)];
            int newLength= (this->Length);
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (modulus%(multiplier*10))/multiplier;
                modulus=modulus-(modulus%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=this->Length;
        return outcome;
    }
    if(this->Length<rhs.Length){
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=rhs.storeLength;
            outcome.valueArray= new int [(rhs.Length)];
            int newLength= (rhs.Length);
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (modulus%(multiplier*10))/multiplier;
                modulus=modulus-(modulus%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=rhs.Length;
        return outcome;
    }
    else{
        bigPosInteger outcome(rhs);
        if (outcome.valueArray != NULL){
            delete []outcome.valueArray;
            outcome.Length=this->storeLength;
            outcome.valueArray= new int [this->Length];
            int newLength= this->Length;
            for(int i=0; i<newLength; newLength--){
                outcome.valueArray[newLength-1] = (modulus%(multiplier*10))/(multiplier);
                modulus=modulus-(modulus%multiplier);
                multiplier=multiplier*10; }}
        outcome.storeoutputLength=this->Length;
        return outcome;
    }
}

bigPosInteger &bigPosInteger::operator=(const bigPosInteger& rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{
    if (this == &rhs) {return *this;}
    else{
        if (valueArray != NULL)
            delete []this->valueArray;
        this->Length=rhs.storeoutputLength;
        valueArray= new int [Length];
        for(int i=0; i<Length; ++i)
            this->valueArray[i] = rhs.valueArray[i];
    return *this;

}
}

std::ostream &operator<<(std::ostream & stream, const bigPosInteger& rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{
    for(int i=0;i<rhs.Length; ++i){
        stream<<rhs.valueArray[i];}
    return stream;
}

std::istream &operator>>(std::istream &stream, bigPosInteger &rhs)
/* this is the copy assignment operator, be EXTREMELY careful for memory leaks here. The default return should be replaced with the appropriate variable*/
{std::string input;
        stream>>input;
    bigPosInteger newClass(input);
    return stream;
    }
